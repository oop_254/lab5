package com.waraporn.week5;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;
public class BubbleSortAppTest {
    @Test
    public void shouldBubbleTestCase1() {
        int arr[] = {5,4,3,2,1};
        int expected[] = {4,3,2,1,5};
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expected,arr);
    }

    @Test
    public void shouldBubbleTestCase2() {
        int arr[] = {4,3,2,1,5};
        int expected[] = {3,2,1,4,5};
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expected,arr);
    }

    @Test
    public void shouldBubbleTestCase3() {
        int arr[] = {3,2,1,4,5};
        int expected[] = {2,1,3,4,5};
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expected,arr);
    }

    @Test
    public void shouldBubbleTestCase4() {
        int arr[] = {2,1,3,4,5};
        int expected[] = {1,2,3,4,5};
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expected,arr);
    }

    @Test
    public void shouldBubbleSortTestCase1() {
        int arr[] = {5,4,3,2,1};
        int expected[] = {1,2,3,4,5};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleSortTestCase2() {
        int arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int sortedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void shouldBubbleSortTestCase3() {
        int arr[] = {1, 9, 8, 10, 6, 5, 4, 3, 2, 7};
        int sortedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void shouldBubbleSortTestCase4() {
        int arr[] = {1, 9, 2, 10, 6, 8, 5, 4, 3, 7};
        int sortedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArray, arr);
    }
}
