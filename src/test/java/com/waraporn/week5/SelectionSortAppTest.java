package com.waraporn.week5;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


import org.junit.Test;
public class SelectionSortAppTest {
    @Test
    public void shouldFindMinIndexTestCase1() {
        int arr[] = {5, 4, 3, 2, 1};
        int pos = 0;
        int minindex = SelectionSortApp.findMinIndex(arr,pos);
        assertEquals(4, minindex);
    }

    @Test
    public void shouldFindMinIndexTestCase2() {
        int arr[] = {1, 4, 3, 2, 5};
        int pos = 1;
        int minindex = SelectionSortApp.findMinIndex(arr,pos);
        assertEquals(3, minindex);
    }

    @Test
    public void shouldFindMinIndexTestCase3() {
        int arr[] = {1, 2, 3, 4, 5};
        int pos = 2;
        int minindex = SelectionSortApp.findMinIndex(arr,pos);
        assertEquals(2, minindex);
    }

    @Test
    public void shouldFindMinIndexTestCase4() {
        int arr[] = {1,1,1,1,1,0,1,1};
        int pos = 0;
        int minindex = SelectionSortApp.findMinIndex(arr,pos);
        assertEquals(5, minindex);
    }

    @Test
    public void shouldSwapTestCase1() {
        int arr[] = {5, 4, 3, 2, 1};
        int expected[] = {1, 4, 3, 2, 5};
        int first = 0;
        int second = 4;
        SelectionSortApp.swap(arr,first,second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldSwapTestCase2() {
        int arr[] = {5, 4, 3, 2, 1};
        int expected[] = {5, 4, 3, 2, 1};
        int first = 0;
        int second = 0;
        SelectionSortApp.swap(arr,first,second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldSelectionSortTestCase1() {
        int arr[] = {5, 4, 3, 2, 1};
        int sortedArray[] = {1, 2, 3, 4, 5};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void shouldSelectionSortTestCase2() {
        int arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int sortedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void shouldSelectionSortTestCase3() {
        int arr[] = {1, 9, 8, 10, 6, 5, 4, 3, 2, 7};
        int sortedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void shouldSelectionSortTestCase4() {
        int arr[] = {1, 9, 2, 10, 6, 8, 5, 4, 3, 7};
        int sortedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }
}
