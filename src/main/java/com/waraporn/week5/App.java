package com.waraporn.week5;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public static int add(int i, int j) {
        return i + j;
    }
}
